
export const EVENTS = [
    {
        id: '1',
        name: 'mDevCamp',
        desc: 'mobile oriented conference',
        timeStarts: 867947279000, // 3/7/1997
        timeEnds: 868206479000, // 6/7/1997
        location: 'Praha',
        ownerName: 'Marek'
    },
    {
        id: '2',
        name: 'Swift aveiro',
        desc: 'swift oriented conference',
        timeStarts: 1528043336000, // 3/6/2018
        timeEnds: 1528388936000, // 7/6/2018
        location: 'Aveiro',
        ownerName: 'Stephan'
    },
    {
        id: '3',
        name: 'try! Swift',
        desc: 'Apple oriented conference',
        timeStarts: 1530548987000, // 2/7/2018
        timeEnds: 1530894587000, // 6/7/2018
        location: 'Tokyo',
        ownerName: 'Marek'
    },
    {
        id: '4',
        name: 'Google IO',
        desc: 'google dev oriented conference',
        timeStarts: 1530548987000, // 2/7/2018
        timeEnds: 1530894587000, // 6/7/2018
        location: 'Bay Area',
        ownerName: 'Sundar'
    },
    {
        id: '5',
        name: 'WWDC',
        desc: 'Apple magic',
        timeStarts: 1530981071000, // 7/7/2018
        timeEnds: 1531240271000, // 10/7/2018
        location: 'San Jose',
        ownerName: 'Phil'
    },
    {
        id: '6',
        name: 'iConference',
        desc: 'mobile developer conf',
        timeStarts: 1533918671000, // 10/8/2018
        timeEnds: 1534091471000, // 12/8/2018
        location: 'Dallas',
        ownerName: 'JR'
    },
    {
        id: '7',
        name: 'UX conference',
        desc: 'fighting for the users!',
        timeStarts: 1533918671000, // 10/8/2018
        timeEnds: 1534091471000, // 12/8/2018
        location: 'Budapest',
        ownerName: 'Bela'
    },
    {
        id: '8',
        name: 'extended GoogleIO',
        desc: 'android related stuff',
        timeStarts: 1533918671000, // 10/8/2018
        timeEnds: 1534091471000, // 12/8/2018
        location: 'Budapest',
        ownerName: 'Bela'
    },
    {
        id: '9',
        name: 'web summit',
        desc: 'what will the web look like?',
        timeStarts: 1533918671000, // 10/8/2018
        timeEnds: 1534091471000, // 12/8/2018
        location: 'London',
        ownerName: 'Jack'
    },
    {
        id: '10',
        name: 'html css',
        desc: 'html cssnference',
        timeStarts: 1533918671000, // 10/8/2018
        timeEnds: 1534091471000, // 12/8/2018
        location: 'Wien',
        ownerName: 'Jack'
    },
    {
        id: '11',
        name: 'build',
        desc: 'Microsoft partypiece',
        timeStarts: 1533918671000, // 10/8/2018
        timeEnds: 1534091471000, // 12/8/2018
        location: 'Seattle',
        ownerName: 'Steve'
    },
    {
        id: '12',
        name: 'Presentation RightNow',
        desc: 'at this very time',
        timeStarts: 1531993113000,
        timeEnds: 1532994113000,
        location: 'Sydney',
        ownerName: 'Austin'
    },
    {
        id: '13',
        name: 'Daily sync',
        desc: 'about daily stuff',
        timeStarts: 1531993113000,
        timeEnds: 1532994113000,
        location: 'Tokyo',
        ownerName: 'Jin'
    },
    {
        id: '14',
        name: 'Conference 9000',
        desc: 'Best conference ever!',
        timeStarts: 1531993113000,
        timeEnds: 1532994113000,
        location: 'New York',
        ownerName: 'Dick'
    },
    {
        id: '15',
        name: 'Almighty meetup',
        desc: 'JS related problems',
        timeStarts: 1531993113000,
        timeEnds: 1532994113000,
        location: 'Vienna',
        ownerName: 'Hanz'
    }

];
