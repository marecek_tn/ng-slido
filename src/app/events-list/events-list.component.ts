import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActionEditEvent } from '../../actions/Actions';
import {Event} from '../../model/Event';
import {EventsService} from '../../services/events.service';
import {Dispatcher} from '../dispatcher';
import {EventModalContent} from '../event-detail/event-detail.component';

@Component({
    selector: 'app-events-list',
    styleUrls: ['./events-list.component.css'],
    templateUrl: './events-list.component.html'
})
export class EventsListComponent implements OnInit, OnDestroy {

    pastEvents: Event[] = [];
    currentEvents: Event[] = [];
    upcomingEvents: Event[] = [];

    constructor(private _eventsService: EventsService,
                private modalService: NgbModal,
                private dispatcher: Dispatcher) {
    }

    ngOnInit() {

        this.pastEvents = this._eventsService.getPastEvents('');
        this.currentEvents = this._eventsService.getCurrentEvents('');
        this.upcomingEvents = this._eventsService.getUpcomingEvents('');

        this._eventsService.pastFiltered.subscribe((events: any) => {
            this.pastEvents = (events as Event[]);
        });

        this._eventsService.currentFiltered.subscribe((events: any) => {
            this.currentEvents = events;
        });

        this._eventsService.upcomingFiltered.subscribe((events: any) => {
            this.upcomingEvents = events;
        });

    }

    ngOnDestroy() {
        this._eventsService.pastFiltered.unsubscribe();
        this._eventsService.currentFiltered.unsubscribe();
        this._eventsService.upcomingFiltered.unsubscribe();
    }

    openEventDetail(event: Event) {
        const modalRef = this.modalService.open(EventModalContent);
        modalRef.componentInstance.title = 'Edit ' + event.name;
        modalRef.componentInstance.setEvent(event);

        modalRef.result.then((result) => {
            // Dispatch adding new event
            const editAction = new ActionEditEvent(
                result.eventId, result.name, result.desc,
                result.timeStarts, result.timeEnds, result.location, result.owner
            );
            this.dispatcher.dispatch(editAction);

        }).catch((error) => {
            console.log('error ' + error);
        });
    }

}
