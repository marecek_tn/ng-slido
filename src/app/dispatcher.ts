
import {Injectable} from '@angular/core';
import {ActionDeleteEvent, ActionEditEvent, ActionEvent, ActionFilterEvent, ActionNewEvent} from '../actions/Actions';
import {EventsService} from '../services/events.service';

@Injectable({
    providedIn: 'root'
})

export class Dispatcher {

    constructor(private _eventService: EventsService) {

    }

    dispatch<A extends ActionEvent>(action: A) {
        // Adding new event
        if (action instanceof ActionNewEvent) {
            const na = <ActionNewEvent> action;
            this._eventService.addNewEvent(na.name, na.desc, +na.timeStarts, +na.timeEnds,
                na.location, na.owner);
        }

        // Editing event
        if (action instanceof ActionEditEvent) {
            const ea = <ActionEditEvent> action;
            this._eventService.updateEvent(ea.id, ea.name, ea.desc,
                +ea.timeStarts, +ea.timeEnds, ea.location, ea.ownerName);
        }

        // Deleting event
        if (action instanceof ActionDeleteEvent) {
            const da = <ActionDeleteEvent> action;
            this._eventService.deleteEvent(da.id);

        }

        // Filtering events
        if (action instanceof ActionFilterEvent) {
            const query = (action as ActionFilterEvent).query;
            this._eventService.filterEvents(query);
        }

    }
}
