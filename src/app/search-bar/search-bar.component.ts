import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActionFilterEvent} from '../../actions/Actions';
import {EventsService} from '../../services/events.service';
import {Dispatcher} from '../dispatcher';

@Component({
    selector: 'app-search-bar',
    styleUrls: ['./search-bar.component.css'],
    templateUrl: './search-bar.component.html'
})
export class SearchBarComponent implements OnInit, OnDestroy {

    searchQuery: string;

    constructor(private _eventService: EventsService,
                private dispatcher: Dispatcher) {
    }

    ngOnInit() {

        this._eventService.searchQuery.subscribe((query: any) => {
            this.searchQuery = query;
        });
    }

    ngOnDestroy() {
        this._eventService.searchQuery.unsubscribe();
    }

    onkey(value: string) {
        const searchAction = new ActionFilterEvent(value);
        this.dispatcher.dispatch(searchAction);
        this.searchQuery = value;
    }

}
