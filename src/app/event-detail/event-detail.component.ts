import {Component} from '@angular/core';
import {FormBuilder, FormGroup } from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActionDeleteEvent, ActionNewEvent} from '../../actions/Actions';
import {Event} from '../../model/Event';
import {Dispatcher} from '../dispatcher';

@Component({
    selector: 'event-modal-content',
    templateUrl: './event-detail.modal.html'
})
export class EventModalContent {

    title: string;
    eventId: string;
    name: string;
    desc: string;
    timeStarts: number;
    timeEnds: number;
    location: string;
    owner: string;
    eventForm: FormGroup;

    constructor(public activeModal: NgbActiveModal,
                private dispatcher: Dispatcher,
                private formBuilder: FormBuilder) {
        this.eventId = '';
        this.createForm();
    }

    setEvent(event: Event) {
        this.eventId = event.id;
        this.eventForm.get('eventId').setValue(event.id);
        this.eventForm.get('name').setValue(event.name);
        this.eventForm.get('desc').setValue(event.desc);
        this.eventForm.get('timeStarts').setValue(event.timeStarts);
        this.eventForm.get('timeEnds').setValue(event.timeEnds);
        this.eventForm.get('location').setValue(event.location);
        this.eventForm.get('owner').setValue(event.ownerName);
    }

    private createForm() {
        this.eventForm = this.formBuilder.group({
            desc: '',
            eventId: '',
            location: '',
            name: '',
            owner: '',
            timeEnds: '',
            timeStarts: ''
        });
    }

    submitForm() {
        this.activeModal.close(this.eventForm.value);
    }

    delete() {
        // Fire delete event
        const eventId = this.eventForm.get('eventId').value;
        this.activeModal.close('Delete clicked');
        this.dispatcher.dispatch(new ActionDeleteEvent(eventId));
    }
}


@Component({
    selector: 'app-event-detail',
    styleUrls: ['./event-detail.component.css'],
    templateUrl: './event-detail.component.html'
})

export class EventDetailComponent {

    constructor(private modalService: NgbModal,
                private dispatcher: Dispatcher) {
    }

    openAddEvent() {
        const modalRef = this.modalService.open(EventModalContent);
        modalRef.componentInstance.title = 'Add new event';

        modalRef.result.then((result) => {
            // Dispatch adding new event
            const newAction = new ActionNewEvent(
                result.name, result.desc, result.timeStarts, result.timeEnds,
                result.location, result.owner
            );
            this.dispatcher.dispatch(newAction);

        }).catch((error) => {
            console.log('error ' + error);
        });
    }
}
