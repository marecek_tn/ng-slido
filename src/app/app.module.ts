import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EventsService} from '../services/events.service';
import {AppComponent} from './app.component';
import {Dispatcher} from './dispatcher';
import {EventDetailComponent, EventModalContent} from './event-detail/event-detail.component';
import {EventsListComponent} from './events-list/events-list.component';
import {SearchBarComponent} from './search-bar/search-bar.component';

@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        EventsListComponent,
        SearchBarComponent,
        EventDetailComponent,
        EventModalContent
    ],
    entryComponents: [EventModalContent],
    imports: [
        BrowserModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [EventsService, Dispatcher]
})
export class AppModule {
}
