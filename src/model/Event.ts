export class Event {

    id: string;
    name: string;
    desc: string;
    timeStarts: number;
    timeEnds: number;
    location: string;
    ownerName: string;

    constructor(id: string, name: string, desc: string,
                timeStarts: number, timeEnds: number, location: string, ownerName: string) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.timeStarts = timeStarts;
        this.timeEnds = timeEnds;
        this.location = location;
        this.ownerName = ownerName;
    }

    searchFor(query: string): boolean {
        if (!query) {
            return true;
        }

        query = query.toLowerCase();
        const searchableFields = ['name', 'desc', 'location', 'ownerName'];
        for (const field of searchableFields) {
            if (this[field].toLowerCase().indexOf(query) !== -1) {
                return true;
            }
        }
        return false;
    }
}

