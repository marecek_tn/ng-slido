import {EventEmitter, Injectable} from '@angular/core';
import {EVENTS} from '../mock-events';
import {Event} from '../model/Event';

@Injectable({
    providedIn: 'root'
})

export class EventsService {

    private pEvents: Event[] = [];
    pastFiltered: EventEmitter<Array<Event>> = new EventEmitter();
    currentFiltered: EventEmitter<Array<Event>> = new EventEmitter();
    upcomingFiltered: EventEmitter<Array<Event>> = new EventEmitter();
    searchQuery: EventEmitter<String> = new EventEmitter<String>();

    constructor() {
        // Load services from mock into 'store'
        for (const event of EVENTS) {
            this.pEvents.push(new Event(event.id, event.name, event.desc,
                event.timeStarts, event.timeEnds,
                event.location, event.ownerName));
        }
    }

    addNewEvent(name: string, desc: string, timeStarts: number, timeEnds: number,
                location: string, ownerName: string) {
        // Calculate new ID
        const newId = Math.random().toString(36).substring(7) + '';
        const event = new Event(newId, name, desc, timeStarts, timeEnds, location, ownerName);
        this.pEvents.push(event);
        // Notify change
        this.filterEvents('');
    }

    updateEvent(id: string, name: string, desc: string, timeStarts: number, timeEnds: number,
                location: string, ownerName: string) {
        const element = this.pEvents.filter((e) => e.id === id)[0];
        const i = this.pEvents.indexOf(element);
        this.pEvents[i] = new Event(id, name, desc, timeStarts, timeEnds, location, ownerName);
        // Notify change
        this.filterEvents('');
    }

    deleteEvent(id: string) {
        this.pEvents = this.pEvents.filter((e) => e.id !== id);
        // Notify change
        this.filterEvents('');
    }

    getPastEvents(query: string): Event[] {
        const result = this.pEvents.filter((event) => event.timeEnds < Date.now())
            .filter((event) => event.searchFor(query));
        this.pastFiltered.emit(result);
        return result;
    }

    getCurrentEvents(query: string): Event[] {
        const result = this.pEvents.filter((event) => (event.timeStarts < Date.now())
            && (event.timeEnds > Date.now()))
            .filter((event) => event.searchFor(query));
        this.currentFiltered.emit(result);
        return result;
    }

    getUpcomingEvents(query: string): Event[] {
        const result = this.pEvents.filter((event) => event.timeStarts > Date.now())
            .filter((event) => event.searchFor(query));
        this.upcomingFiltered.emit(result);
        return result;
    }

    filterEvents(query: string) {
        this.getPastEvents(query);
        this.getCurrentEvents(query);
        this.getUpcomingEvents(query);
        this.searchQuery.emit(query);
    }
}
