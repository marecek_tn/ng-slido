
export abstract class ActionEvent {
}

export class ActionNewEvent extends ActionEvent {
    constructor(public name: string,
                       public desc: string,
                       public timeStarts: string,
                       public timeEnds: string,
                       public location: string,
                       public owner: string) {
        super();
    }
}

export class ActionEditEvent extends ActionEvent {
    constructor(public id: string,
                       public name: string,
                       public desc: string,
                       public timeStarts: string,
                       public timeEnds: string,
                       public location: string,
                       public ownerName: string) {
        super();
    }
}

export class ActionDeleteEvent extends ActionEvent {
    constructor(public id: string) {
        super();
    }
}

export class ActionFilterEvent extends ActionEvent {
    constructor(public query: string) {
        super();
    }
}
